/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.jpa;

import if6ae.entity.Inscricao;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author Giusepe
 */
public class InscricaoJpa extends JpaController{
    public InscricaoJpa(){
        
    }
    
    public List<Inscricao> findAll() {
        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<Inscricao> q = em.createQuery(
                "select i from Inscricao i order by i.numero",
                Inscricao.class);
            return q.getResultList();
        } finally {
            if (em != null) em.close();
        }
    }
    
    public List<Inscricao> findByNumero(Integer numero) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<Inscricao> q = em.createQuery(
                    "SELECT i FROM Inscricao WHERE i.numero = :numero ORDER BY i.nome",
                    Inscricao.class);
            q.setParameter("numero", numero);
            return q.getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public List<Inscricao> findByCpf(Long cpf) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<Inscricao> q = em.createQuery(
                    "SELECT i FROM Inscricao WHERE i.cpf = :cpf ORDER BY i.nome",
                    Inscricao.class);
            q.setParameter("cpf", cpf);
            return q.getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
