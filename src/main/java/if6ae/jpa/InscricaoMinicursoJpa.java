/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.jpa;

import if6ae.entity.Inscricao;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author Giusepe
 */
public class InscricaoMinicursoJpa extends JpaController{
    
    public InscricaoMinicursoJpa(){
        
    }
    
    public List<Inscricao> findInscricaoMinicursoByNumero(Integer numero) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<Inscricao> q = em.createQuery(
                    "SELECT im FROM InscricaoMinicurso im WHERE im.inscricao.numero = :numero",
                    Inscricao.class);
            q.setParameter("numero", numero);
            return q.getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
